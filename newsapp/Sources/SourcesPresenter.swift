//
//  SourcesPresenter.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import Foundation

protocol SourcesView: NSObjectProtocol {
    func setSourcesList(data: Array<[String:Any]>)
}

class SourcesPresenter{
    weak fileprivate var sourceView: SourcesView?
    
    static let sharedInstance = SourcesPresenter()
    
    func attachView(view: SourcesView){
        self.sourceView = view
    }
    
    func getNewsSourcesList(query: String, category: String){
        APIManager.sharedInstance.getNewsSources(category: category, success: { (data) in
            let data = data["sources"] as? Array<[String:Any]> ?? []
            self.sourceView?.setSourcesList(data: query == "" ? data : data.filter({($0["name"] as? String ?? "").lowercased().contains(query.lowercased())}))
        }) { (error) in
            print(error)
        }
    }
}
