//
//  ArticlesViewController.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit
import Kingfisher

class ArticlesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ArticlesView{
    @IBOutlet weak var articlesTableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    var sourcesName: String? = nil
    var sources: String? = nil
    var query: String? = nil
    var page: Int = 1
    var items: Array<[String:Any]> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ArticlesPresenter.sharedInstance.attachView(view: self)
        ArticlesPresenter.sharedInstance.getNewsArticlesList(query: query ?? "", sources: sources ?? "", page: page)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar(title: sourcesName ?? "")
    }
    
    //MARK: Presenter Delegate
    func setArticlesList(data: Array<[String : Any]>) {
        self.items.append(contentsOf: data)
        self.articlesTableView.reloadData()
    }
    
    func setLastPage(page: Int) {
        self.page = page
    }
    
    //MARK: TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toDetails", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 {
            let activityIndicator = UIActivityIndicatorView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            activityIndicator.startAnimating()
            tableView.backgroundView = activityIndicator
            tableView.separatorStyle = .none
            return 0
        } else {
            tableView.backgroundView = nil
            tableView.separatorStyle = .singleLine
            return items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.articlesTableView.dequeueReusableCell(withIdentifier: "articles", for: indexPath) as! ArticlesTableViewCell
        
        cell.titleLabel.text = self.items[indexPath.row]["title"] as? String ?? ""
        
        let dateFormatted = self.formatDate(date: self.items[indexPath.row]["publishedAt"] as? String ?? "")
        cell.dateLabel.text =  dateFormatted
        
        let url = self.items[indexPath.row]["urlToImage"] as? String ?? "\(Constant.imageBaseUrl)seed/\((items[indexPath.row]["title"] as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")/600"
        let resource = ImageResource(downloadURL: URL(string: url)!, cacheKey: url)
        cell.articlesImageView.kf.indicatorType = .activity
        cell.articlesImageView.kf.setImage(with: resource)
        
        return cell
    }
    
    //MARK: Scrollview Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.articlesTableView{
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom == height {
                ArticlesPresenter.sharedInstance.getNewsArticlesList(query: query ?? "", sources: sources ?? "", page: page)
            }
        }
    }
    
    //MARK: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.query = textField.text
        self.page = 1
        self.items.removeAll()
        self.articlesTableView.reloadData()
        
        ArticlesPresenter.sharedInstance.getNewsArticlesList(query: query ?? "", sources: sources ?? "", page: page)
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetails"{
            let details = segue.destination as? DetailViewController
            details?.stringTitle = items[sender as! Int]["title"] as? String ?? ""
            details?.stringUrl = items[sender as! Int]["url"] as? String ?? "https://google.com"
        }
    }
}
