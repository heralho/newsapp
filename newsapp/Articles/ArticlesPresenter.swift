//
//  ArticlesPresenter.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import Foundation

protocol ArticlesView: NSObjectProtocol {
    func setArticlesList(data: Array<[String:Any]>)
    func setLastPage(page: Int)
}

class ArticlesPresenter{
    weak fileprivate var articlesView: ArticlesView?
    
    static let sharedInstance = ArticlesPresenter()
    
    func attachView(view: ArticlesView){
        self.articlesView = view
    }
    
    func getNewsArticlesList(query: String, sources: String, page: Int){
        APIManager.sharedInstance.getNewsArticles(query: query, sources: sources, page: page, success: { (data) in
            let data = data["articles"] as? Array<[String:Any]> ?? []
            if !data.isEmpty{
                self.articlesView?.setArticlesList(data: data)
                self.articlesView?.setLastPage(page: page + 1)
            }
        }) { (error) in
            print(error)
        }
    }
}
