//
//  APIManager.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import Foundation
import Alamofire

class APIManager{
    static let sharedInstance = APIManager()
    
    func getNewsSources(category: String, success: @escaping (([String:Any]) -> Void), error: @escaping ((String) -> Void)){
        AF.request(Constant.SourcesUrl + "&category=\(category)").responseJSON { (response) in
            switch response.result {
            case .success:
                let JSON = response.value as? [String:Any] ?? [:]
                success(JSON)
                break
            case .failure:
                error("failed")
                break
            }
        }
    }
    
    func getNewsArticles(query:String, sources: String, page: Int, success: @escaping (([String:Any]) -> Void), error: @escaping ((String) -> Void)){
        AF.request(Constant.ArticlesUrl + "&sources=\(sources)&q=\(query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)&page=\(page)&pageSize=10").responseJSON { (response) in
            switch response.result {
            case .success:
                let JSON = response.value as? [String:Any] ?? [:]
                print(JSON)
                success(JSON)
                break
            case .failure:
                print("failed")
                error("failed")
                break
            }
        }
    }
}
