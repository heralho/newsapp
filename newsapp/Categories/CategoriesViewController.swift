//
//  ViewController.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit
import Kingfisher

class CategoriesViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var categoriesFlowLayout: UICollectionViewFlowLayout!
    
    var items: [String] = ["business","entertainment","general","health","science","sports","technology"].shuffled()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar(title: "News Category")
    }
    
    //MARK: CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toSources", sender: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "categories", for: indexPath) as! CategoriesCollectionViewCell
        
        cell.titleLabel.text = items[indexPath.row].capitalized
        
        let url = "\(Constant.imageBaseUrl)seed/\(items[indexPath.row])/600"
        let resource = ImageResource(downloadURL: URL(string: url)!, cacheKey: url)
        cell.imageView.kf.indicatorType = .activity
        cell.imageView.kf.setImage(with: resource)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width: CGFloat?
        var height: CGFloat?
        if indexPath.row == 0 {
            width = collectionView.frame.size.width
            height = collectionView.frame.size.width / 2
        } else {
            width = collectionView.frame.size.width / 2
            height = width
        }
        return CGSize(width: width!, height: height!)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSources"{
            let sources = segue.destination as? SourcesViewController
            sources?.category = items[sender as! Int]
        }
    }
}

