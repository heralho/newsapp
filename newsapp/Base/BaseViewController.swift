//
//  BaseViewController.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setNavigationBar(title: String){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = title
    }
    
    func formatDate(date: String) -> String{
        let formatter = DateFormatter()//2020-04-14T22:41:37Z
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let date = formatter.date(from: date)
        
        formatter.dateFormat = "dd MMMM yyyy HH:mm"
        let dateFormatted = formatter.string(from: date ?? Date())
        
        return dateFormatted
    }
}
