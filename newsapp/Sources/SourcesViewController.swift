//
//  SourcesViewController.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit
import Kingfisher

class SourcesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SourcesView{
    @IBOutlet weak var sourcesTableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    var category: String? = nil
    var query: String? = nil
    var items: Array<[String:Any]> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SourcesPresenter.sharedInstance.attachView(view: self)
        SourcesPresenter.sharedInstance.getNewsSourcesList(query: "", category: category ?? "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar(title: category?.capitalized ?? "")
    }
    
    //MARK: Presenter Delegate
    func setSourcesList(data: Array<[String : Any]>) {
        self.query?.removeAll()
        self.items.removeAll()
        self.items.append(contentsOf: data)
        self.sourcesTableView.reloadData()
    }
    
    //MARK: TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toArticles", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 {
            let activityIndicator = UIActivityIndicatorView.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            activityIndicator.startAnimating()
            tableView.backgroundView = activityIndicator
            tableView.separatorStyle = .none
            return 0
        } else {
            tableView.backgroundView = nil
            tableView.separatorStyle = .singleLine
            return items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.sourcesTableView.dequeueReusableCell(withIdentifier: "sources", for: indexPath) as! SourcesTableViewCell
        
        cell.titleLabel.text = self.items[indexPath.row]["name"] as? String ?? ""
        cell.descLabel.text = self.items[indexPath.row]["description"] as? String ?? ""
        
        let url = "\(Constant.imageBaseUrl)seed/\((items[indexPath.row]["name"] as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")/\(Int(self.sourcesTableView.frame.size.width))/120"
        let resource = ImageResource(downloadURL: URL(string: url)!, cacheKey: url)
        cell.sourcesImageView.kf.indicatorType = .activity
        cell.sourcesImageView.kf.setImage(with: resource)
        
        return cell
    }
    
    //MARK: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.query = textField.text
        SourcesPresenter.sharedInstance.getNewsSourcesList(query: query ?? "", category: category ?? "")
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toArticles"{
            let articles = segue.destination as? ArticlesViewController
            articles?.sources = items[sender as! Int]["id"] as? String ?? ""
            articles?.sourcesName = items[sender as! Int]["name"] as? String ?? ""
        }
    }
}

//Catatan
//pada endpoint API ini tidak ada parameter untuk pagination.
