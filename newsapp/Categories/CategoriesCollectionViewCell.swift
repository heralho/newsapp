//
//  CategoriesCollectionViewCell.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var blackView: UIView!
}
