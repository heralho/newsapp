//
//  DetailViewController.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: BaseViewController, WKNavigationDelegate, WKUIDelegate {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var stringTitle: String? = nil
    var stringUrl: String? = nil
    var isNavEnable: WKNavigationActionPolicy = .allow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        
        self.loadWebview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar(title: stringTitle ?? "")
    }
    
    func loadWebview(){
        let url = URL(string: stringUrl ?? "https://google.com")
        let urlRequest = URLRequest(url: url!)
        webView.load(urlRequest)
        
        self.activityIndicator.startAnimating()
    }
    
    //MARK: WKWebView Delegate    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.stopAnimating()
        self.isNavEnable = .cancel
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(self.isNavEnable)
    }

}
