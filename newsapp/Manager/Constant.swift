//
//  Constant.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import Foundation

struct Constant {
    static let API_Key = "6aed458bc4a046b387f94e6e1a694821"
    static let imageBaseUrl = "https://picsum.photos/"
    static let baseUrl = "https://newsapi.org/v2/"
    
    //Endpoint
    static let SourcesUrl = baseUrl + "sources?apikey=" + API_Key
    static let ArticlesUrl = baseUrl + "everything?apikey=" + API_Key
}
