//
//  SourcesTableViewCell.swift
//  newsapp
//
//  Created by Heraldy Dwifany on 15/04/20.
//  Copyright © 2020 Heraldy Dwifany. All rights reserved.
//

import UIKit

class SourcesTableViewCell: UITableViewCell {
    @IBOutlet weak var sourcesImageView: UIImageView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
